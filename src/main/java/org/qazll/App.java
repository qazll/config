package org.qazll;

import com.alibaba.fastjson2.JSON;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        System.out.println(Long.toString(config, 2));
        config(Type.A);
        System.out.println("A:" + checkConfig(Type.A) + "   B:" + checkConfig(Type.B) + "   C:" + checkConfig(Type.C));
        System.out.println(Long.toString(config, 2));
        config(Type.B);
        System.out.println("A:" + checkConfig(Type.A) + "   B:" + checkConfig(Type.B) + "  C:" + checkConfig(Type.C));
        System.out.println(Long.toString(config, 2));
        config(Type.C);
        System.out.println("A:" + checkConfig(Type.A) + "   B:" + checkConfig(Type.B) + "   C:" + checkConfig(Type.C));
        System.out.println(Long.toString(config, 2));
        deConfig(Type.B);
        System.out.println("A:" + checkConfig(Type.A) + "   B:" + checkConfig(Type.B) + "  C:" + checkConfig(Type.C));
        System.out.println(Long.toString(config, 2));
        config(Type.A,Type.C,Type.B);
        System.out.println("A:" + checkConfig(Type.A) + "   B:" + checkConfig(Type.B) + "  C:" + checkConfig(Type.C));
        System.out.println(Long.toString(config, 2));
        sul sul = new sul();
        sul.setType(Type.A);
        System.out.println(JSON.toJSONString(sul));
    }



    private static Long config = 0L;

    public static void config(Type... types) {
        for (Type type : types) {
            config |= type.index;
        }
    }

    public static void deConfig(Type type) {
        config &= ~type.index;
    }

    public static boolean checkConfig(Type type) {
        return (config & type.index) != 0;
    }

    public enum Type {
        A(1L),
        B(1L << 1),
        C(1L << 2);
        public final long index;
        Type(long index) {
            this.index = index;
        }
    }


}

class sul{
    private App.Type type;
    private final int a = 1;

    public App.Type getType() {
        return type;
    }

    public void setType(App.Type type) {
        this.type = type;
    }
}