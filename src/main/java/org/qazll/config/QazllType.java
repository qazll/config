package org.qazll.config;


public enum QazllType {
    A(1), B(2), C(3), D(4);

    private int index;
    QazllType(int i) {
        setIndex(i);
    }
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
