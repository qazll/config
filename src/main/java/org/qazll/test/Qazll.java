package org.qazll.test;

import org.qazll.config.ConstantType;
import org.qazll.config.QazllType;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@ConstantType
public class Qazll {

    private QazllType type;

    protected void defaut() {
        try {
            Class<? extends Qazll> aClass = this.getClass();
            boolean annotationPresent = aClass.isAnnotationPresent(ConstantType.class);
            if (annotationPresent) {
                ConstantType annotation = aClass.getAnnotation(ConstantType.class);
                QazllType qazllType = annotation.type();
                Method method = aClass.getMethod("setType", QazllType.class);
                method.invoke(this, qazllType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(type);
    }

    public void display() {
        defaut();
    }

    public QazllType getType() {
        return type;
    }

    public void setType(QazllType type) {
        this.type = type;
    }
}
